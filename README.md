
# Wawancara LPDP

Repositori yang digunakan dalam rangka wawancara tenaga TIK LPDP

## Soal Wawancara LPDP

1.  Terdapat DB SQL berisi 2 tabel: tabel **kategori_makanan** dan tabel **makanan** dengan relasi 1-to-many. LPDP telah menyiapkan file **wawancara_lpdp.sql** (untuk MySQL/Postgre/SQL Server) maupun **wawancara_lpdp.bak** (khusus SQL Server) berisikan kedua tabel tersebut. Anda dapat melakukan import/restore database tersebut pada laptop/PC masing-masing, dan Anda bebas dapat menggunakan DBMS MySQL maupun SQL Server.
2.  Struktur dari kedua tabel tsb kira-kira seperti ini

Tabel **kategori_makanan**

| kategori_makanan_id | kategori_makanan |
|--|--|
| 1 | Buah |
| 2 | Sayur |
| 3 | Makanan Pokok |
| dst.. | dst.. |

Tabel **makanan**

| makanan_id | kategori_makanan_id | makanan |
|--|--|--|
| 1 | 1 | Jeruk |
| 2 | 1 | Apel |
| 3 | 2 | Kangkung |
| 4 | 2 | Bayam |
| 5 | 3 | Nasi |
| 6 | 3 | Sagu |
| dst.. | dst.. | dst.. |

**Keterangan**  
_makanan.kategori_makanan_id_ merupakan foreign key ke _kategori_makanan.kategori_makanan_id_

3. Yang perlu dilakukan peserta adalah, cukup buat 1 halaman/webpage menggunakan Laravel/Yii2/NET Core untuk menampilkan tabel seperti berikut:

| Kategori Makanan | Makanan |
|--|--|
| Buah | Jeruk |
| Buah | Apel |
| Sayur | Kangkung |
| Sayur | Bayam |
| Makanan Pokok | Nasi |
| Makanan Pokok | Sagu |
| dst.. | dst.. |

## Cara Pengerjaan - Menggunakan Gitlab

| No | Skenario |
|--|--|
| 1 | Peserta melakukan pembuatan direktori baru pada repositori tersebut. Pembuatan direktori tersebut akan membuat fork pada repositori tersebut, sehingga membuat repositori baru dengan URL: [https://gitlab.com/NAMA_PESERTA/wawancara-lpdp.git](https://gitlab.com/NAMA_PESERTA/wawancara-lpdp.git). |
| 2 | Peserta melakukan clone dari repositori hasil fork di nomor 2 |
| 3 | Peserta mengerjakan soal dalam waktu yang ditentukan (60 menit) di laptop/PC masing-masing. Pastikan untuk menjadikan direktori yang telah dibuat di nomor 2 tadi sebagai root folder pengerjaan soal |
| 4 | Setelah waktu 60 menit habis, push seluruh hasil pengerjaan ke repositori, dan buat merge request |
| 5 | Peserta akan diberikan waktu 10 menit untuk demo dan memberikan penjelasan singkat mengenai code yang telah dibuat |

## Cara Pengerjaan - Menggunakan Cara Manual

| No | Skenario |
|--|--|
| 1 | Peserta mengerjakan soal dalam waktu yang ditentukan (60 menit) di laptop/PC masing-masing. |
| 2 | Setelah waktu 60 menit habis, upload seluruh hasil pengerjaan ke URL: [20220916](https://kemenkeu-my.sharepoint.com/:f:/g/personal/rhezatama_kemenkeu_go_id/Ev7GAJCvEMVFu9qY3YFD1VkBqmZjA0SBVrwTceFiDqy3cw?e=CLlhXK). Mohon untuk membuat folder dengan menggunakan nama lengkap sebagai nama folder, lalu diupload ke folder yang sudah dibuat tersebut |
| 3 | Peserta akan diberikan waktu 10 menit untuk demo dan memberikan penjelasan singkat mengenai code yang telah dibuat |

## Kriteria Penilaian

1.  Ketepatan output
2.  Kecepatan pengerjaan
3.  Detail penjelasan singkat
4.  `Tidak wajib, tapi menjadi bonus apabila dipenuhi` Tabel memiliki fungsi filtering
5.  `Tidak wajib, tapi menjadi bonus apabila dipenuhi` Tabel memiliki fungsi CRUD lengkap
6.  `Tidak wajib, tapi menjadi bonus apabila dipenuhi` Peserta dapat menyediakan endpoint REST API GET untuk masing-masing tabel, yang berfungsi mengembalikan seluruh data dalam tabel seperti berikut:

endpoint **/kategori_makanan**

    {
	    {
			id:1,
			kategori_makanan:buah
		},
		{
			id:2,
			kategori_makanan:sayur
		},
    }

endpoint **/makanan**

    {
	    {
			id:1,
			kategori_makanan_id:1,
			makanan:jeruk
		},
		{
			id:2,
			kategori_makanan_id:1,
			makanan:apel
		},
		{
			id:3,
			kategori_makanan_id:2,
			makanan:kangkung
		},
    }



